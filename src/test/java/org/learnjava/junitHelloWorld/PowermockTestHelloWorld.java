package org.learnjava.junitHelloWorld;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.*;

public class PowerMockTestHelloWorld
{

    @Test
    public void testStaticMethod () throws Exception
    {
        String expectedResult = "a static method";

        // We register a new instance of test class under test as usually.

        // This is the way to tell PowerMock
        // to mock all static methods of a given class.
        mockStatic(HelloWorld.class);

        /*
         * The static method call to HelloWorld.dummyStaticMethod() expectation.
         * This is why we need PowerMock.
         */
        // Note how we replay the class, not the instance.
        replay(HelloWorld.class);

        String actualResult = HelloWorld.dummyStaticMethod();

        // Note how we verify the class, not the instance!
        verify(HelloWorld.class);

        //Assert that the result is correct.
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testPrivateMethod () throws Exception
    {
        final String privateMethodName = "dummyPrivateMethod";
        final String expectedResult = "a private method";

        // Mock only the private method
        HelloWorld helloWorldTestPrivate =
                createPartialMock(HelloWorld.class, privateMethodName);

        //Expect the private method call to "dummyPrivateMethod"
        expectPrivate(helloWorldTestPrivate, privateMethodName).andReturn(true);

        replay(helloWorldTestPrivate);

        //assertEquals(helloWorldTestPrivate);

        verify(helloWorldTestPrivate);
    }
}
