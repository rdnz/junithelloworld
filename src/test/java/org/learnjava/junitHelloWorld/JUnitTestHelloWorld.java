package org.learnjava.junitHelloWorld;

/**
 * Set language level to 5.0 to make test file work.
 */

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class JUnitTestHelloWorld
{

    private HelloWorld h;

    @Before
    public void setUp()
            throws Exception
    {
        h = new HelloWorld();
    }

    @Test
    public void testHelloEmpty()
            throws Exception
    {
        assertEquals(h.getName(),"");
        assertEquals(h.getMessage(),"Hello!");
    }

    @Test
    public void testHelloWorld()
            throws Exception
    {
        h.setName("World");
        assertEquals(h.getName(),"World");
        assertEquals(h.getMessage(),"Hello World!");
    }
}

