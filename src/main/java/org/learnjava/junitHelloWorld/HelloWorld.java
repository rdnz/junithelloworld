package org.learnjava.junitHelloWorld;

import java.util.LinkedList;


/**************************************************************************
 * We need a class to cover void, static, public, protected, private,
 * asynchronous method, thread safe, with final argument.
 * Test code is in the same package of this class.
 *************************************************************************/
public class HelloWorld
{

    private String name = "";
    private LinkedList<String> friends;
    private String Zip;
    private String lastFriendPrivate;

    protected String getLastFriendProtected()
    {
        return friends.getLast();
    }

    private String getLastFriendPrivate()
    {
        return friends.getLast();
    }

    public String getName()
    {
        return name;
    }

    public String getFirstFriend()
    {
        return friends.get(0);
    }

    public final String getFirstFriendFinal()
    {
        return friends.get(0);
    }

    public final String getZip()
    {
        return Zip;
    }

    public String getMessage()
    {
        if (name.equals("")) {
            return "Hello!";
        } else {
            return "Hello " + name + "!";
        }
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setFriends(final LinkedList friends)
    {
        this.friends = new LinkedList(friends);
    }

    public void setZip(final String zip) {
        this.Zip = zip;
    }

    /**
     * This is a dummy void method just for testing.
     * Void methods need a special way to test in Mockito.
     */
    public void dummyVoidMethod()
    {

    }

    /**
     * This is a static method just for testing.
     * General comment: static methods are bad!
     *
     * @return a string.
     */
    public static String dummyStaticMethod()
    {
        return "a static method";
    }

    /**
     *
     * @return
     */
    // A dummy private method for test.
    private String dummyPrivateMethod()
    {
        return "a private method";
    }
}
