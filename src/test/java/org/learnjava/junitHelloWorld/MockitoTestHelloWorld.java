package org.learnjava.junitHelloWorld;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

public class MockitoTestHelloWorld
{
    HelloWorld helloWorldRealObject;
    @Before
    public void before()
            throws Exception
    {
        helloWorldRealObject = new HelloWorld();
        helloWorldRealObject.setName("realObjectName");
    }

    /**
     * Simple test with real object.
     * @throws Exception
     */
    @Test
    public void testHelloWorldUsingRealObject()
            throws Exception
    {
        String helloWorldString = helloWorldRealObject.getMessage();

        assertEquals(helloWorldString, "Hello realObjectName!");

        /**********************************************************************
         * A real object CANNOT be stubbed.
         *********************************************************************/
        /*
        // This WILL FAIL:
        when(helloWorld.getMessage()).thenReturn("Hello tryStubRealObject!");
        assertEquals(helloWorld.getMessage(), "Hello tryStubRealObject!");
        */
    }

    /**
     * Test with mock().
     * Mock is a "test spy". Test spy uses "arrange, act, assert" pattern.
     *
     * @throws Exception
     */
    @Test
    public void testHelloWorldUsingMock()
            throws Exception
    {
        /**********************************************************************
         * Create a mock by passing the real object class to mock method.
         *********************************************************************/
        HelloWorld helloWorldMock = mock(HelloWorld.class);

        /**********************************************************************
         * For a mock, all its methods will return null be default.
         * we need to stub the methods to be tested.
         * Real method DOES NOT stub a mock.
         *********************************************************************/

        given(helloWorldMock.getMessage()).willReturn("Hello mockName!");
        assertEquals(helloWorldMock.getMessage(), "Hello mockName!");

        /**********************************************************************
         * Stubbing can be overridden.
         *********************************************************************/

        when(helloWorldMock.getMessage()).thenReturn("Hello newMockName!");
        assertEquals(helloWorldMock.getMessage(), "Hello newMockName!");

        /**********************************************************************
         * Stubbed method does NOT throw exception when real method does.
         *********************************************************************/
        // The following line will throw NullPointerException exception.
        //assertEquals(helloWorldRealObject.getFirstFriend(), "");

        // The stubbed method will NOT throw exception.
        when(helloWorldMock.getFirstFriend()).thenReturn("mockedFirstFriend");
        assertEquals(helloWorldMock.getFirstFriend(), "mockedFirstFriend");

        /**********************************************************************
         * Mockito DOES NOT mock final methods.
         * Stubbing a final method will not take effect but typically will
         * throw a NullPointerException because the field is not initiated.
         *********************************************************************/
        // The following line does not take effect.
        when(helloWorldMock.getZip()).thenReturn("60615");
        // Notice: getZip() STILL returns null.
        assertEquals(helloWorldMock.getZip(), null);

        // The following line will throw NullPointerException:
        //when(helloWorldMock.getFirstFriendFinal()).thenReturn("firstFriend");
        // The following line will throw NullPointerException too:
        //doReturn("firstFriend").when(helloWorldMock).getFirstFriendFinal();

        /**********************************************************************
         * Private methods CANNOT be mocked by Mockito.
         *********************************************************************/

        /**********************************************************************
         * CANNOT invoke real methods of a mock. (I hate this stupid idea.)
         * Invoking real method of a mock will not take effect.
         *********************************************************************/
        // The following line does not take effect.
        helloWorldMock.setName("doesNotWork");
        // Notice: getName() STILL returns null.
        assertEquals(helloWorldMock.getName(), null);


        /**********************************************************************
         * Static methods cannot be mocked by Mockito.
         *
         * org.mockito.exceptions.misusing.MissingMethodInvocationException:
         * when() requires an argument which has to be 'a method call on a mock'.
         * For example:
         * when(mock.getArticles()).thenReturn(articles);
         *
         * Also, this error might show up because:
         * 1. you stub either of: final/private/equals()/hashCode() methods.
         * Those methods *cannot* be stubbed/verified.
         * 2. inside when() you don't call method on mock but on some other object.
         * 3. the parent of the mocked class is not public.
         * It is a limitation of the mock engine.
         *********************************************************************/
        // The following line would cause error:
        // when(HelloWorld.dummyStaticMethod()).thenReturn("try stub static return");
    }

    /**
     * Test with spy().
     * Spy is partial mocking and calls real methods unless they are stubbed.
     *
     * @throws Exception
     */
    @Test
    public void testHelloWorldUsingSpy()
            throws Exception
    {
        /**********************************************************************
         * Create a spy by passing the real object INSTANCE to spy method.
         * You CANNOT pass a mock object to spy method.
         *********************************************************************/
        HelloWorld helloWorldSpy = spy(helloWorldRealObject);

        /**********************************************************************
         * The methods of a spy object are real methods by default.
         *********************************************************************/
        assertEquals(helloWorldSpy.getMessage(), "Hello realObjectName!");

        /**********************************************************************
         * Real methods work on spy object.
         *********************************************************************/
        helloWorldSpy.setName("realSpyName");
        assertEquals(helloWorldSpy.getMessage(), "Hello realSpyName!");

        /**********************************************************************
         * "Partial mocking": stub only some methods of a spy object.
         *********************************************************************/
        when(helloWorldSpy.getName()).thenReturn("stubbedSpyName");
        assertEquals(helloWorldSpy.getName(), "stubbedSpyName");
        assertEquals(helloWorldSpy.getMessage(), "Hello realSpyName!");

        /**********************************************************************
         * Spy method DOES throw the same exception when real method does.
         *********************************************************************/
        // The following line will throw NullPointerException exception.
        //assertEquals(helloWorldSpy.getFirstFriend(), "");

        /**********************************************************************
         * Mockito DOES NOT mock final methods.
         * Stubbing a final method will not take effect but typically will
         * throw a NullPointerException because the field is not initiated.
         *********************************************************************/
        // The following line does not take effect.
        when(helloWorldSpy.getZip()).thenReturn("60615");
        // getZip() still returns null.
        assertEquals(helloWorldSpy.getZip(), null);

        // The following line will throw NullPointerException:
        //when(helloWorldSpy.getFirstFriendFinal()).thenReturn("firstFriend");
    }

    /**************************************************************************
     * Test void methods with Mockito.
     * To stub void method, use:
     * doThrow(new IllegalArgumentException()).when(someObject).voidMethod();
     * Also works for spy.
     *************************************************************************/
    @Test (expected = IllegalArgumentException.class)
    public void testVoidMethodShouldThrowIllegalArgumentException()
    {
        HelloWorld helloWorldMock = mock(HelloWorld.class);

        doThrow(new IllegalArgumentException("Bad argument!"))
                .when(helloWorldMock).dummyVoidMethod();
        helloWorldMock.dummyVoidMethod();

        HelloWorld helloWorldSpy = spy(helloWorldRealObject);

        doThrow(new IllegalArgumentException("Bad argument!"))
                .when(helloWorldSpy).dummyVoidMethod();
        helloWorldSpy.dummyVoidMethod();
    }
}
